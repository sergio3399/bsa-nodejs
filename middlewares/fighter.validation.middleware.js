const { fighter } = require('../models/fighter');
const FighterSevice = require('../services/fighterService');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    try{
        if(!req.body.health) {
            req.body.health = 100;
        }
        validateFieldsExist(req.body);
        validateFighterExist(req.body);
        validateName(req.body);
        validateHealth(req.body);
        validatePower(req.body);
        validateDefense(req.body);
    } catch (error) {
        res.status(400).error = error;
    } finally {
        next();
    }
}

const validateFieldsExist = body => {
    const {id, ...otherKeys} = fighter;
    const neededKeys = Object.keys(otherKeys).sort().join();
    const bodyKeys = Object.keys(body).sort().join();

    if(neededKeys !== bodyKeys) {
        throw `Expected or Unexpected fields. Props have only ${neededKeys}, if health is empty, it will get a standard value 100 `;
    }
}

const validateFighterExist = body => {
    if (FighterSevice.search({name: body.name})) {
        throw `This name ${body.name} already used`;
    }
}

const validateName = body => {
    if (body.name.replace(/\s/g, '').lenght < 1) {
        throw 'Name value is empty';
    }
}

const validateHealth = body => {
    if (body.health < 80 || body.health > 120 || isNaN(body.health)) {
        throw 'Health must be number 80 < health < 120';
    }
}

const validatePower = body => {
    if(body.power < 1 || body.power > 100 || isNaN(body.power)) {
        throw 'Power must be number 1 < power < 100';
    }
}

const validateDefense = body => {
    if(body.defense < 1 || body.defense > 10 || isNaN(body.defense)) {
        throw 'Defense must be number 1 < defense < 10';
    }
}
const updateFighterValid = (req, res, next) => {
    try {
        let isUpdate = false;
        const {id, ...neededKeys} = fighter;
        const bodyKeys = Object.keys(req.body);

        if (!FighterSevice.search({id: req.params.id})) { 
            throw `There is no fighter with id ${req.params.id}`;
        }

        bodyKeys.forEach((element) => {
            if (!neededKeys.hasOwnProperty(element)) {
                throw `Unexpected ${element} fields`;
            }
        });

        if (req.body.name) {
            validateName(req.body);
            isUpdate = true;
        } 

        if (req.body.health) {
            validateHealth(req.body);
            isUpdate = true;
        } 

        if (req.body.power) {
            validatePower(req.body);
            isUpdate = true;
        } 

        if (req.body.defense) {
            validateDefense(req.body);
            isUpdate = true;
        } 
        
        if(!isUpdate) {
          throw 'No one filds to update';
        }
        
    } catch (error) {
        res.status(400).error = error;
    } finally {
        next();
    }
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;