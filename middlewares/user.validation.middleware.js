const { user } = require('../models/user');
const UserService = require('../services/userService');
const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    try {
        validateFieldsExist(req.body);
        validateUserName(req.body);
        validateEmail(req.body.email);
        validatePhoneNumber(req.body.phoneNumber);
        validateUserExist(req.body);
        validatePassword(req.body.password);
    } catch (error) {
        res.status(400).error = error;
    } finally {
        next();
    }
}

const validateFieldsExist = body => {
    const {id, ...otherKeys} = user;
    const neededKeys = Object.keys(otherKeys).sort().join();
    const bodyKeys = Object.keys(body).sort().join();

    if(neededKeys !== bodyKeys) {
        throw `Expected or Unexpected fields. Props have only ${neededKeys}`;
    }
}

const validatePassword = password => {
    if (password && password.length < 3) {
        throw 'the password must be at least three characters long';
    }
}

const validateEmail = email => {
    let re = /^[a-z0-9](\.?[a-z0-9]){3,}@[Gg][Mm][Aa][Ii][Ll]\.com$/;
    if (email && !re.test(email)) {
        throw 'Not a valid Gmail email address example: user@gmail.com';
    }
}

const validatePhoneNumber = phoneNumber => {
    let re = /^\+380\d{9}$/;
    if(phoneNumber && !re.test(phoneNumber)) {
        throw 'Not a valid phone number. Phone number should be +380xxxxxxxxx';
    }
}

const validateUserName = body => {
    if(body.lastName && body.lastName.replace(/\s/g, '').length < 1){
        throw 'Last name can`t be empty';
    }
    if(body.firstName && body.firstName.replace(/\s/g, '').length < 1){
        throw 'First name can`t be empty';
    }
}

const validateUserExist = body => {
    if (UserService.search({email: body.email})) { 
        throw `This email ${body.email} already used`;
    }

    if (UserService.search({phoneNumber: body.phoneNumber})) {
        throw `This phone number ${body.phoneNumber} already used`;
    }
}

const updateUserValid = (req, res, next) => {
    try {
        const {id, ...neededKeys} = user;
        const bodyKeys = Object.keys(req.body);

        bodyKeys.forEach((element) => {
            if (!neededKeys.hasOwnProperty(element)) {
                throw `Unexpected ${element} fields`;
            }
        });

        if (!UserService.search({id: req.params.id})) { 
            throw `There is no user with id ${req.params.id}`;
        }

        if(!isBodyHasFieldToUpdate(bodyKeys, neededKeys)) {
            throw 'No one filds to update';
        }


        validateUserExist(req.body);
        validateUserName(req.body);
        validatePhoneNumber(req.body.phoneNumber);
        validateEmail(req.body.email);
        validatePassword(req.body.password);

    } catch (error) {
        res.status(400).error = error;
    } finally {
        next();
    }
}

const isBodyHasFieldToUpdate = (bodyKeys, neededKeys) => {
    let isUpdate = false;
    bodyKeys.forEach(element => {
        if (neededKeys.hasOwnProperty(element)) {
            isUpdate = true;
        } 
    });
    return isUpdate;
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;