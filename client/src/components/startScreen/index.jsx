import * as React from 'react';
import SignInUpPage from '../signInUpPage';
import { isSignedIn } from '../../services/authService';
import Fight from '../fight';
import SignOut from '../signOut';
import History from '../fightHistory'
import { Button } from '@material-ui/core';
import FightScreen from '../fightScreen';


class StartScreen extends React.Component {
    state = {
        isSignedIn: false,
        isHistory: false,
        isFightStarted: false,
        selectedFighters: []
    };

    openHistory = () => {
        this.setState({
            isHistory: true,
        });
    };
    
    closeHistory = () => {
        this.setState({
            isHistory: false,
        });
    };

    componentDidMount() {
        this.setIsLoggedIn(isSignedIn());
    }

    setIsLoggedIn = isSignedIn => {
        this.setState({ isSignedIn });
    }

    setIsFightStarted = isFightStarted => {
        this.setState({ isFightStarted })
    }

    setSelectedFighters = selectedFighters => {
        this.setState({ selectedFighters })
    }

    render() {
        const { isSignedIn, isHistory, isFightStarted, selectedFighters} = this.state;
        if (!isSignedIn) {
            return <SignInUpPage setIsLoggedIn={this.setIsLoggedIn} />
        }

        if (isHistory) {
            return <History closeHistory={this.closeHistory.bind(this)}/>
        }

        if(isFightStarted){
            return <FightScreen 
                selectedFighters={selectedFighters}
                setIsFightStarted={this.setIsFightStarted} 
            />
        }

        return (
            <>
                <Button variant="contained" color="primary">
                    <div onClick={this.openHistory}>History</div>
                </Button>
                <Fight
                    setIsFightStarted={this.setIsFightStarted} 
                    setSelectedFighters={this.setSelectedFighters}
                />
                <SignOut isSignedIn={isSignedIn} onSignOut={() => this.setIsLoggedIn(false)} />
            </>
        );
    }
}

export default StartScreen;