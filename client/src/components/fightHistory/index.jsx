import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { getFights } from '../../services/domainRequest/fightRequest';
import { Button } from '@material-ui/core';
import Row from './tableRow'
import './fightHistory.css'

export default class History extends React.Component {

    state = {
        fights: []
    }

    async componentDidMount() {
        const fights = await getFights();
        this.setState({fights});
    }

    render() {
        const {closeHistory} = this.props;
        return ( 
            <TableContainer component={Paper}>
                <div className="btn-wrapper">
                    <Button onClick={closeHistory} variant="contained" color="primary">Back to Start menu</Button>
                </div>
                <Table aria-label="collapsible table">
                    <TableHead>
                        <TableRow>
                            <TableCell />
                            <TableCell align="center">id</TableCell>
                            <TableCell align="center">First fighter name</TableCell>
                            <TableCell align="center">Second fighter name</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.state.fights.map(row => (
                            <Row key={row.name} row={row} />
                        ))}
                    </TableBody>
                </Table>
            </TableContainer> 
        );
    }
}