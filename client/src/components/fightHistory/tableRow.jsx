import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';



const useRowStyles = makeStyles({
  root: {
    '& > *': {
      borderBottom: 'unset',
    },
  },
});


export default function Row(props) {
  const { row } = props;
  const [open, setOpen] = React.useState(false);
  const classes = useRowStyles();

  return (
    <React.Fragment>
      <TableRow className={classes.root}>
        <TableCell>
          <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
            {open ? '↑'  : '↓' }
          </IconButton>
        </TableCell>
        <TableCell align="center">{row.id}</TableCell>
        <TableCell align="center">{row.fighter1}</TableCell>
        <TableCell align="center">{row.fighter2}</TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1}>
              <Typography variant="h6" gutterBottom component="div">
                Log
              </Typography>
              <Table size="small" aria-label="purchases">
                <TableHead>
                  <TableRow>
                    <TableCell align="center">First fighter attack</TableCell>
                    <TableCell align="center">Second fighter attack</TableCell>
                    <TableCell align="center">First fighter health</TableCell>
                    <TableCell align="center">Second fighter health</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {row.log.map((logRow) => (
                    <TableRow key={logRow.fighter1Shot}>
                      <TableCell align="center">{logRow.fighter1Shot}</TableCell>
                      <TableCell align="center">{logRow.fighter2Shot}</TableCell>
                      <TableCell align="center">{logRow.fighter1Health}</TableCell>
                      <TableCell align="center">{logRow.fighter2Health}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

Row.propTypes = {
  row: PropTypes.shape({
    id: PropTypes.string.isRequired,
    fighter1: PropTypes.string.isRequired,
    fighter2: PropTypes.string.isRequired,
    log: PropTypes.arrayOf(
      PropTypes.shape({
        fighter1Shot: PropTypes.number.isRequired,
        fighter2Shot: PropTypes.number.isRequired,
        fighter1Health: PropTypes.number.isRequired,
        fighter2Health: PropTypes.number.isRequired,
      }),
    ).isRequired,
    name: PropTypes.string.isRequired,
  }).isRequired,
};
