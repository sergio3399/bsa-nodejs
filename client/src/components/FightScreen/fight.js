import { controls } from '../../constants/controls';


export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const playerOne = {
      ...firstFighter,
      health: parseInt(firstFighter.health, 10),
      maxHealth: firstFighter.health,
      isAttacked: false,
      isInBlock: false,
      isCriticalHitAllowed: true,
      position: 'left'
    }

    const playerTwo = {
      ...secondFighter,
      health: parseInt(secondFighter.health, 10),
      maxHealth: secondFighter.health,
      isAttacked: false,
      isInBlock: false,
      isCriticalHitAllowed: true,
      position: 'right'
    }

    const fightLog = {
      id: new Date(),
      fighter1: firstFighter.name,
      fighter2: secondFighter.name,
      log: [        
        {
            fighter1Shot: 0,
            fighter2Shot: 0,
            fighter1Health: firstFighter.health,
            fighter2Health: secondFighter.health
        }
      ]
    }
    
    let pressed = new Set();

    window.addEventListener('keydown', event => {

      pressed.add(event.code);
      switch (event.code) {
        case controls.PlayerOneAttack:
          if (validateAttack(playerOne, playerTwo)) {
            playerOne.isAttacked = true;
            const damageOne = getDamage(playerOne, playerTwo);
            playerTwo.health -= damageOne;

            fightLog.log.push({
              fighter1Shot: damageOne.toFixed(2),
              fighter2Shot: 0,
              fighter1Health: playerOne.health.toFixed(2),
              fighter2Health: playerTwo.health.toFixed(2)
            });
          }
          break;

        case controls.PlayerTwoAttack:
          if (validateAttack(playerTwo, playerOne)) {
            playerTwo.isAttacked = true;
            const damageTwo = getDamage(playerTwo, playerOne);
            playerOne.health -= damageTwo;

            fightLog.log.push({
              fighter1Shot: 0,
              fighter2Shot: damageTwo.toFixed(2),
              fighter1Health: playerOne.health.toFixed(2),
              fighter2Health: playerTwo.health.toFixed(2)
            });
          }
          break;

        case controls.PlayerOneBlock:
          playerOne.isInBlock = true;
          break;

        case controls.PlayerTwoBlock:
          playerTwo.isInBlock = true;
          break;
      }

      if (controls.PlayerOneCriticalHitCombination.every(code => pressed.has(code))){
        if (playerOne.isCriticalHitAllowed) {
          const damageOne = getCriticalHit(playerOne);
          playerTwo.health -= damageOne;
          fightLog.log.push({
            fighter1Shot: damageOne.toFixed(2),
            fighter2Shot: 0,
            fighter1Health: playerOne.health.toFixed(2),
            fighter2Health: playerTwo.health.toFixed(2)
          });       
          playerOne.isCriticalHitAllowed = false;     
          setTimeout(function () {
            playerOne.isCriticalHitAllowed = true;
          }, 10000);
        }
      }

      if (controls.PlayerTwoCriticalHitCombination.every(code => pressed.has(code))){
        if (playerTwo.isCriticalHitAllowed) {
          const damageTwo = getCriticalHit(playerTwo);
          playerOne.health -= damageTwo;
          fightLog.log.push({
            fighter1Shot: 0,
            fighter2Shot: damageTwo.toFixed(2),
            fighter1Health: playerOne.health.toFixed(2),
            fighter2Health: playerTwo.health.toFixed(2)
          });   
          playerTwo.isCriticalHitAllowed = false;  
          setTimeout(function () {
            playerTwo.isCriticalHitAllowed = true;
          }, 10000);
        }
      }

      changeHealthBar(playerOne.position, playerOne.health, playerOne.maxHealth);
      changeHealthBar(playerTwo.position, playerTwo.health, playerTwo.maxHealth);

      if (playerOne.health <= 0) {
        resolve({winner: secondFighter, fightLog});
      }
      if (playerTwo.health <= 0) {
        resolve({winner: firstFighter, fightLog});
      }
    })

    window.addEventListener('keyup', event => {
      pressed.delete(event.code);
      switch (event.code) {
        case controls.PlayerOneAttack:
          playerOne.isAttacked = false;
          break;

        case controls.PlayerTwoAttack:
          playerTwo.isAttacked = false;
          break;

        case controls.PlayerOneBlock:
          playerOne.isInBlock = false;
          break;

        case controls.PlayerTwoBlock:
          playerTwo.isInBlock = false;
          break;
      }
    })

  });
}
function validateAttack(attacker, defender) {
  return !attacker.isAttacked && !attacker.isInBlock && !defender.isInBlock;
}

function changeHealthBar(indicatorPosition, playerHp, maxPlayerHp) {
  const healthIndicator = document.getElementById(`${indicatorPosition}-fighter-indicator`);
  const hpSecondFighterRemains = playerHp < 0 ? 0 : (playerHp / maxPlayerHp) * 100;
  healthIndicator.style.width = `${hpSecondFighterRemains}%`;
}


export function getDamage(attacker, defender) {
  const damageRaw = getHitPower(attacker) - getBlockPower(defender);
  const damage = damageRaw > 0 ? damageRaw : 0;
  return damage;
}

export function getCriticalHit(fighter) {
  return 2 * fighter.power;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  const power = fighter.power * criticalHitChance;
  return power;
  
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  const power = fighter.defense * dodgeChance;
  return power;
}
