import * as React from 'react';
import './fightScreen.css'
import { fight } from './fight';
import {createFight} from '../../services/domainRequest/fightRequest';
import Modal from '@material-ui/core/Modal';
import Box from '@material-ui/core/Box';


class FightScreen extends React.Component {

    state = {
        isModalOpen: false,
        winner: null
    }

    async componentDidMount() {
        const {winner, fightLog} = await fight(...this.props.selectedFighters);
        this.setState({
            isModalOpen: true,
            winner
        })
        
        createFight(fightLog);
    }

    handleClose () {
        this.setState({isModalOpen: false})
        this.props.setIsFightStarted(false);
    }


    render() {
        const fighter1 = this.props.selectedFighters[0];
        const fighter2 = this.props.selectedFighters[1];
        const defaultImage = "https://i.pinimg.com/originals/c0/53/f2/c053f2bce4d2375fee8741acfb35d44d.gif";

        return (
                <div class="arena___root">
                    <Modal
                        open={this.state.isModalOpen}
                        onClose={this.handleClose.bind(this)}
                        aria-labelledby="simple-modal-title"
                        aria-describedby="simple-modal-description"
                    >
                        <>
                        <p className="help_text">To close, click outside the modal window</p>
                        <Box className="winner_wrapper">
                            <h2>Winner {this.state.winner && this.state.winner.name}</h2>
                            <img src={defaultImage} className="fighter-preview___img" alt="winner img"/>
                        </Box>
                        </>
                    </Modal>
                    <div className="arena___fight-status">
                        <div className="arena___fighter-indicator">
                            <span className="arena___fighter-name">{fighter1.name}</span>
                            <div className="arena___health-indicator">
                                <div className="arena___health-bar" id="left-fighter-indicator"></div>
                            </div>
                        </div>
                        <div className="arena___versus-sign">VS</div>
                        <div className="arena___fighter-indicator">
                            <span className="arena___fighter-name">{fighter2.name}</span>
                            <div className="arena___health-indicator">
                                <div className="arena___health-bar" id="right-fighter-indicator"></div>
                            </div>
                        </div>
                    </div>
                    <div className="arena___battlefield">
                        <div className="arena___fighter arena___left-fighter">
                            <img src={defaultImage} alt="fighterImg" className="fighter-preview___img" />
                        </div>
                        <div className="arena___fighter arena___right-fighter">
                            <img src={defaultImage} alt="fighterImg2" className="fighter-preview___img" />
                        </div>
                    </div>
                </div>
        );
    }
}

export default FightScreen;