const { Router } = require('express');
const FightService = require('../services/fightService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();

// OPTIONAL TODO: Implement route controller for fights
router.get('/', (req, res, next) => {
        res.data = FightService.getAll();
        next();
}, responseMiddleware);

router.post('/', (req, res, next) => {
    if(res.error){
        return next();
    }
    res.data = FightService.create(req.body);
    next();
}, responseMiddleware);

module.exports = router;