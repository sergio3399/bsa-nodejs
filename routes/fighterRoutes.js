const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter
router.get('/', (req, res, next) => {
        res.data = FighterService.getAll();
        next();
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        const fighter = FighterService.search({id: req.params.id});
        if(!fighter) {
            throw `There is no fighter with id ${search.id}`;
        }
        res.data = fighter;    
    } catch (error) {
        res.status(404).error = error;
    } finally {
        next();
    }
}, responseMiddleware);

router.post('/', createFighterValid, (req, res, next) => {
    if(res.error){
        return next();
    }
    res.data = FighterService.create(req.body);
    next();
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
    if(res.error){
        return next();
    }
    res.data = FighterService.update(req.params.id, req.body);
    next();
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    if(res.error){
        return next();
    }
    res.data = FighterService.delete(req.params.id);
    next();
}, responseMiddleware);

module.exports = router;